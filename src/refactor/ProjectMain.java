package src.refactor;

public class ProjectMain {

    public static void main(String args[]){
        entregar(EnumTipoEntrega.gerarValorRandomico());
    }

    public static void entregar(EnumTipoEntrega enumTipoEntrega){
        var logistica = new FabricaLogistica().buscarObjeto(enumTipoEntrega);
        logistica.realizarEntrega();
    }
    
}
