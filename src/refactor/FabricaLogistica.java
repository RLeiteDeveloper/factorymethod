package src.refactor;

import java.lang.reflect.InvocationTargetException;

public class FabricaLogistica {
    
    public Logistica buscarObjeto(EnumTipoEntrega enumTipoEntrega){
       
        try {   

            var nomeObjetoFormatado  = "src.refactor.".concat(enumTipoEntrega.getDescricao());
            Class<?> cls = Class.forName(nomeObjetoFormatado);
            return (Logistica) cls.getDeclaredMethod("novo").invoke(cls.getDeclaredConstructor().newInstance());
            
        } catch (IllegalAccessException | InstantiationException |  IllegalArgumentException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | SecurityException e) {
            e.printStackTrace();
        }

        return null; 
      
    }

    
}
