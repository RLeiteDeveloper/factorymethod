package src.refactor;

import java.util.Random;

public enum EnumTipoEntrega {
  
    CAMINHAO(1, "Caminhao"), NAVIO(2,"Navio");

    private final int valor;
    private final String descricao;

    EnumTipoEntrega(int valor, String descricao){
      this.valor = valor;
      this.descricao  = descricao;
    }

    public int getValor(){
        return valor;
    }

    public String getDescricao(){
      return descricao;
  }

    public static EnumTipoEntrega gerarValorRandomico() {
      var values = EnumTipoEntrega.values();
      var length = values.length;
      var randIndex = new Random().nextInt(length);
      return values[randIndex];
  }
}
